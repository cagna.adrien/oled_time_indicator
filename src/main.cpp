#include <Wire.h>

byte time_data[3];
byte second, minute, hour;

int i = 0;


void setup() {
  // put your setup code here, to run once:

  Wire.begin();

  delay(1);

  // RTC control register initialization
  Wire.beginTransmission(0x68);
  Wire.write(0x07);
  Wire.write(0x00);
  Wire.endTransmission();

  // starting the clock
  Wire.beginTransmission(0x68);
  Wire.write(0x00);
  Wire.write(0x00);
  Wire.endTransmission();

}

void loop() {
  // put your main code here, to run repeatedly:

  // set the register pointer to second register
  Wire.beginTransmission(0x68);
  Wire.write(0x00);
  Wire.endTransmission();

  // read 3 bytes from the second register
  Wire.requestFrom(0x68, 3);

  while(Wire.available())
  {
    time_data[i] = Wire.read();
    i++;
  }

  // convert BCD format data from the RTC to decimal format
  second = (time_data[0] & 0x0f) + (((time_data[0] & 0x70)>>4)*10);
  minute = (time_data[1] & 0x0f) + (((time_data[1] & 0x70)>>4)*10);
  hour = (time_data[2] & 0x0f) + (((time_data[2] & 0x10)>>4)*10);

  i = 0;

  delay(1000);
}
